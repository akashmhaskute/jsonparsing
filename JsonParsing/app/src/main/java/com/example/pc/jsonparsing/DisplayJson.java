package com.example.pc.jsonparsing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DisplayJson extends AppCompatActivity {

    JSONObject jsonObject;
    JSONArray jsonArray;
    ListView listView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_json);
        listView=findViewById(R.id.list_item_View);
        ArrayList <String>  arrayList=new ArrayList<>();

        String jsonIp=getIntent().getExtras().getString("json");
        Toast.makeText(this, ""+jsonIp, Toast.LENGTH_SHORT).show();

        try
        {
            jsonObject=new JSONObject(jsonIp);

            jsonArray=jsonObject.getJSONArray("results");
            int cnt=0;

            while(cnt < jsonObject.length())
            {
                JSONObject jsonObject1=jsonArray.getJSONObject(cnt);

                JSONObject jsonObject2=jsonObject1.getJSONObject("geometry");

                JSONObject jsonObject3=jsonObject2.getJSONObject( "location");

                String lat=jsonObject3.getString("lat");
                String lag=jsonObject3.getString("lng");

                cnt++;
                Log.i("DATA","lat="+lat);
                Log.i("DATA","lag="+lag);
                arrayList.add("lat:"+lat+"\n"+"long:"+lag);


            }

            ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayList);
            listView.setAdapter(arrayAdapter);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}
