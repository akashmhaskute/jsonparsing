package com.example.pc.jsonparsing;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pc on 3/15/2018.
 */

public class Location
{
    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private  double lng;

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
