package com.example.pc.jsonparsing;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pc on 3/15/2018.
 */

public class Geometry
{
    @SerializedName("location")
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


}
