package com.example.pc.jsonparsing;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity
{
    String jsonIp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void getJson(View view)
    {
        new JsonParsing().execute();
    }

    public void jsonParsing(View view)
    {
          if(jsonIp ==null)
          {
              Toast.makeText(this, "jsonIp is null", Toast.LENGTH_SHORT).show();
          }
          else
          {
              Intent intent=new Intent(this,DisplayJson.class);
              intent.putExtra("json",jsonIp);
              startActivity(intent);
          }
    }


   class JsonParsing extends AsyncTask <Void,Void,String>
    {
        String uri;

        @Override
        protected void onPreExecute()
        {
            uri="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670,151.1957&radius=500&types=food&name=cruise&key=AIzaSyCHNKPWwKaO84CyKV7TDDiuz5pMw0ISxRk";
        }

        @Override
        protected String doInBackground(Void... voids)
        {

            String holdLine;
            StringBuilder stringBuilder=new StringBuilder();
            try
            {
                URL url=new URL(uri);
                HttpURLConnection httpURLConnection=(HttpURLConnection) url.openConnection();
                InputStream inputStream=httpURLConnection.getInputStream();
                DataInputStream dataInputStream=new DataInputStream(inputStream);



                while((holdLine=dataInputStream.readLine())!=null)
                {
                    stringBuilder.append(holdLine+"\n");
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            return stringBuilder.toString();
        }

        @Override
        protected void onPostExecute(String s)
        {
            TextView textView=findViewById(R.id.text);
            textView.setText(s);
            jsonIp=s;


        }
    }
}
